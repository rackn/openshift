---
Name: openshift-cluster-tools
Description: A task to prepare OpenShift cluster tools and essential resources
Documentation: |
  # OpenShift Cluster Tools Task

  This task prepares essential OpenShift tools and resources required for cluster
  deployment and management. It handles installation of CLI tools, SSH key generation,
  and basic cluster resource configuration.

  ## Requirements

  ### Parameters
  - openshift/installer-url: OpenShift installer download URL
  - openshift/oc-url: OpenShift CLI tools download URL
  - openshift/cluster-domain: Base domain for cluster
  - openshift/pull-secret: Red Hat pull secret

  ### Infrastructure
  - Network access to tool downloads
  - Sufficient storage for binaries
  - DNS resolution for cluster domain
  - Valid Red Hat credentials

  ## Usage

  ### Tool Installation
  ```bash
  # Apply tools task
  drpcli machines tasks add <machine-uuid> openshift-cluster-tools

  # Verify installations
  openshift-install version
  oc version
  ```

  ### Task Operations
  1. Generates cluster SSH keys
  2. Downloads and installs:
     - OpenShift installer
     - OpenShift CLI (oc)
     - Kubernetes CLI (kubectl)
  3. Configures DNS resolution
  4. Sets up environment variables

Meta:
  color: yellow    # Yellow for tool installation tasks
  feature-flags: sane-exit-codes
  icon: wrench    # Wrench icon represents tools/utilities
  type: os       # Operating system level task

OptionalParams:
  - openshift/installer-url
  - openshift/oc-url
  - openshift/cluster-domain

RequiredParams:
  - openshift/pull-secret

ExtraClaims:
  - action: '*'
    scope: 'profiles'
    specific: '*'

Templates:
  - Name: openshift-cluster-ssh-key.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      mkdir -p /root/.ssh
      chmod 700 /root/.ssh

      {{ if .ParamExists "openshift/ssh-key" }}
      job_success "Keys already exist"
      {{ end }}

      if ! command_exists ssh-keygen ; then
        dnf install -y openssh openssl gpgme-devel which
      fi

      cd /root
      job_info "Create cluster ssh key"
      ssh-keygen -t ed25519 -N '' -f /root/.ssh/cluster_ssh_key

      job_info "Store the key parts"
      drpcli profiles set {{.Machine.Name}} param openshift/ssh-key to - < /root/.ssh/cluster_ssh_key >/dev/null
      drpcli profiles set {{.Machine.Name}} param openshift/ssh-key-pub to - < /root/.ssh/cluster_ssh_key.pub >/dev/null

      job_success "${0} Complete"

  - Name: openshift-cluster-configre-resolv-conf.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      job_info "Update the resolv.conf to point at DRP"
      echo "nameserver $(drpcli info get | jq .address -r)" > /etc/resolv.conf
      echo "domain {{.Machine.Name}}.{{.ParamExpand "openshift/cluster-domain"}}" >> /etc/resolv.conf

      job_success "${0} Complete"

  - Name: openshift-install-tools.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      if ! command_exists openshift-install ; then
        job_info "Installing the openshift-installer"
        curl -fsSL "{{ .ParamExpand "openshift/installer-url" }}" -o /root/openshift-installer.tgz
        tar -zxvf /root/openshift-installer.tgz
        mv openshift-install /usr/local/bin
        rm /root/openshift-installer.tgz
      fi

      if ! command_exists oc ; then
        job_info "Installing the oc" # don't call it that!
        curl -fsSL "{{ .ParamExpand "openshift/oc-url" }}" -o /root/oc-installer.tgz
        tar -zxvf /root/oc-installer.tgz
        mv oc kubectl /usr/local/bin
        rm /root/oc-installer.tgz
      fi

      job_info "writing 'export KUBECONFIG=/root/cluster/auth/kubeconfig' to ~/.bashrc"

      echo 'export KUBECONFIG=/root/cluster/auth/kubeconfig' | tee -a ~/.bashrc

      job_success "${0} Complete"

  - Name: openshift-cluster-registry-setup.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      {{ if .ParamExists "openshift/external-registry" }}

      {{ if .ParamExpand "openshift/external-registry-create"}}
      {{ end }}

      {{ end }}

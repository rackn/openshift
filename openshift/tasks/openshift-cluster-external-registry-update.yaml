---
Name: openshift-cluster-external-registry-update
Description: A task to update and populate an external registry with required OpenShift container images
Documentation: |
  # External Registry Update Task

  This task updates and populates an external registry with required OpenShift container images.
  It's essential for disconnected installations, ensuring all necessary container images
  are available locally for cluster operation.

  ## Requirements

  ### Parameters
  - openshift/external-registry: External registry URL
  - openshift/external-registry-update: Boolean flag to enable update
  - openshift/oc-mirror-url: URL for oc-mirror tool
  - openshift/pull-secret: Red Hat registry credentials

  ### Infrastructure
  - Existing external registry service
  - Sufficient storage capacity
  - Network access to Red Hat registries (for initial mirroring)
  - Valid registry certificates

  ## Usage

  ### Basic Update
  ```bash
  # Enable registry update
  drpcli machines set <machine-uuid> param openshift/external-registry-update to true

  # Apply update task
  drpcli machines tasks add <machine-uuid> openshift-cluster-external-registry-update

  # Verify mirrored images
  oc-mirror list --from-registry <registry-url>
  ```

  ### Update Process
  1. Installs oc-mirror tool
  2. Configures authentication
  3. Initializes registry metadata
  4. Mirrors required images:
     - OpenShift release images
     - UBI base images
     - Serverless images
     - Service mesh images

Meta:
  color: yellow    # Yellow for update/sync operations
  feature-flags: sane-exit-codes
  icon: sync      # Sync icon represents update/refresh
  type: os       # Operating system level task

ExtraClaims:
  - action: '*'
    scope: 'profiles'
    specific: '*'
  - action: '*'
    scope: 'resource_brokers'
    specific: '*'
  - action: '*'
    scope: 'machines'
    specific: '*'

Templates:
  - Name: openshift-cluster-registry-update.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      {{ if .ParamExists "openshift/external-registry" }}

      drpcli machines set $RS_UUID param registry/cert to - < <(drpcli resource_brokers get Name:registry-service param registry/cert --decode --aggregate | jq -r | jq -sR) >/dev/null

      {{ if .ParamExpand "openshift/external-registry-update"}}

      if ! command_exists oc-mirror ; then
        job_info "Installing the oc-mirror"
        curl -fsSL "{{ .ParamExpand "openshift/oc-mirror-url" }}" -o /root/oc-installer.tgz
        tar -zxvf /root/oc-installer.tgz
        mv oc-mirror /usr/local/bin
        chmod +x /usr/local/bin/oc-mirror
        rm /root/oc-installer.tgz
      fi

      # TODO: MAKE THIS COME FROM CREATE and change default
      PASSWORD=$(echo -n "admin:admin" | base64 -w0)

      job_info "Build auth file"
      mkdir -p ~/.docker
      cat > ~/.docker/config.json <<EOF
      {
        "auths": {
          "{{.ParamExpand "openshift/external-registry"}}": {
            "auth": "$PASSWORD",
            "email": "admin@local.local"
          },
      EOF
      echo '{{.ParamExpand "openshift/pull-secret" | toJson}}' | jq .auths | tail -n +2 >> ~/.docker/config.json
      cat >> ~/.docker/config.json <<EOF
      }
      EOF

      job_info "Initial registry"
      oc mirror init --registry {{.ParamExpand "openshift/external-registry"}}/mirror/oc-mirror-metadata > imageset-config.yaml
      sed -i 's/skipTLS: false/skipTLS: true/g' imageset-config.yaml
      job_info "Update/Load registry"
      oc mirror --config=./imageset-config.yaml docker://{{.ParamExpand "openshift/external-registry"}} --dest-skip-tls

      {{ else }}
      job_info "Update registry is false ... Skipping"
      {{ end }}

      {{ end }}

      job_success "${0} Complete"

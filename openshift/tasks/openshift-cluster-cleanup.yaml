---
Name: openshift-cluster-cleanup
Description: A task to cleanup extra resources of a openshift cluster
Documentation: |
  # OpenShift Cluster Cleanup Task

  This task performs a comprehensive cleanup of OpenShift cluster resources and associated
  infrastructure components. It ensures proper removal of cluster-related resources to
  prevent orphaned components and resource leaks.

  ## Requirements

  ### Permissions
  - Cluster administrator access
  - Zone management permissions
  - Machine management access
  - Profile modification rights

  ### Cluster State
  - Cluster should be in a state where cleanup is safe
  - Workloads should be properly drained
  - Data backups should be completed if needed

  ## Usage

  ### Basic Cleanup
  ```bash
  # Apply cleanup task
  drpcli machines tasks add <machine-uuid> openshift-cluster-cleanup

  # Verify zone removal
  drpcli zones show <cluster-name>
  ```

  ### Cleanup Process
  1. Removes DNS zone entries
  2. Cleans up infrastructure components
  3. Removes cluster-specific profiles and parameters

Meta:
  color: yellow    # Yellow for cleanup/maintenance tasks
  feature-flags: sane-exit-codes
  icon: trash     # Trash icon represents cleanup/removal
  type: os       # Operating system level task

ExtraClaims:
  - action: '*'
    scope: 'zones'
    specific: '*'
OptionalParams: []
Prerequisites: []
RequiredParams: []
Templates:
  - Name: openshift-cluster-cleanup.sh
    Contents: |
      #!/usr/bin/env bash

      {{ template "setup.tmpl" . }}

      drpcli zones destroy {{.Machine.Name}} >/dev/null 2>/dev/null || true

      exit 0

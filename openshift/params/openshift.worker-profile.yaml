---
Name: openshift/worker-profile
Description: Profile name for OpenShift worker node configuration
Documentation: |
  # Worker Node Profile

  This parameter specifies which profile to apply to worker nodes.
  Worker nodes run application workloads and provide resources for
  user applications and container deployments.

  ## Requirements
  - Must reference a valid DRP profile
  - Profile must contain necessary worker node configuration
  - Only applied to nodes with worker role

  ## Usage
  The default profile is 'openshift-worker' which configures:
  ```bash
  # Network services
  - HTTP ingress (port 80)
  - HTTPS ingress (port 443)

  # HAProxy backend services
  - ingress-router-80
  - ingress-router-443
  ```

  Notes:
  - Worker nodes can have varying hardware specifications
  - Scale worker nodes based on application requirements
  - Profile includes HAProxy configurations for application ingress
  - Used in conjunction with openshift/role=worker
  - Former bootstrap node becomes a worker node after initialization

Meta:
  color: blue      # Blue for compute/worker resources
  icon: cog        # Cog icon represents worker/compute functions
  title: Worker Profile Configuration

Schema:
  type: string
  default: openshift-worker

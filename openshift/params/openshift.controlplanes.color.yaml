---
Name: openshift/controlplanes/color
Description: UI color designation for control plane nodes
Documentation: |
  # Control Plane Node Color

  This parameter defines the color used to represent control plane nodes
  in the Digital Rebar UI. This visual indicator helps quickly identify
  control plane nodes in the interface.

  ## Requirements
  - Must be a valid Fomantic UI color name
  - Should be consistent across installations
  - Should provide good visual contrast
  - Should align with node role significance

  ## Usage
  Default color configuration:
  ```bash
  # Standard setting
  yellow    # High visibility for critical components

  # Common alternatives
  red       # Critical infrastructure
  orange    # Core services
  ```

  Notes:
  - Used only for UI presentation
  - Helps identify node types visually
  - Supports operational awareness
  - No functional impact on cluster

Meta:
  color: yellow    # Yellow for control plane nodes
  icon: paint brush # Paint brush icon represents color setting
  title: Control Plane UI Color

Schema:
  type: string
  default: yellow

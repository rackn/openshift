---
Name: openshift/kubeconfig
Description: Cluster authentication and configuration file for kubectl/oc tools
Documentation: |
  # Cluster Kubeconfig

  This parameter stores the kubeconfig file containing authentication and
  configuration details for accessing the OpenShift cluster via CLI tools.
  This file is critical for cluster management and operations.

  ## Requirements
  - Must contain valid cluster certificates
  - Must include proper authentication tokens
  - Must be protected from unauthorized access
  - Must contain correct API endpoints

  ## Usage
  Configuration storage:
  ```bash
  # Default location during installation
  /root/cluster/auth/kubeconfig

  # Provides configuration for:
  - API server endpoints
  - Authentication details
  - Certificate authorities
  - Context information
  - Cluster access credentials
  ```

  Notes:
  - Generated during cluster installation
  - Contains sensitive cluster access details
  - Required for CLI operations
  - Should be backed up securely
  - Used by oc and kubectl commands

Meta:
  color: red       # Red for security/authentication
  icon: key file   # Key file icon represents secure configuration
  title: Cluster Access Configuration
  password: "true"     # Indicates sensitive content

Schema:
  type: object

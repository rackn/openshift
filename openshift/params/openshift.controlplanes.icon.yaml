---
Name: openshift/controlplanes/icon
Description: UI icon designation for control plane nodes
Documentation: |
  # Control Plane Node Icon

  This parameter defines the icon used to represent control plane nodes
  in the Digital Rebar UI. This visual indicator helps quickly identify
  control plane nodes and their function in the interface.

  ## Requirements
  - Must be a valid Fomantic UI icon name
  - Should be intuitive for role purpose
  - Should be consistent across installations
  - Should provide clear visual identification

  ## Usage
  Default icon configuration:
  ```bash
  # Standard setting
  caret square right    # Represents control/management

  # Common alternatives
  server               # Infrastructure services
  sitemap              # Control plane topology
  microchip           # Core processing
  ```

  Notes:
  - Used only for UI presentation
  - Helps identify node purpose visually
  - Supports operational awareness
  - No functional impact on cluster

Meta:
  color: yellow    # Yellow for control plane components
  icon: pencil     # Pencil icon represents customization setting
  title: Control Plane UI Icon

Schema:
  type: string
  default: caret square right

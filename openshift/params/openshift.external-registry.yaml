---
Name: openshift/external-registry
Description: External registry URL for disconnected OpenShift installations
Documentation: |
  # External Registry Configuration

  This parameter specifies the URL of an external container registry for 
  disconnected or air-gapped OpenShift installations. When set, the cluster
  will use this registry instead of Red Hat's public registries.

  ## Requirements
  - Must be a valid container registry URL
  - Registry must be accessible from all cluster nodes
  - Must have sufficient storage for OpenShift images
  - Must support both HTTP and HTTPS
  - Must have required OpenShift images mirrored

  ## Usage
  Configure the registry URL:
  ```bash
  # Example formats:
  registry.example.com             # Standard hostname
  registry.example.com:5000        # Custom port
  192.168.1.100:5000              # IP address

  # Registry will serve:
  - OpenShift release images
  - Red Hat core images
  - UBI base images
  - OpenShift serverless images
  - Service mesh images
  ```

  Notes:
  - Required for disconnected installations
  - Works with openshift/external-registry-create
  - Used by openshift/external-registry-update
  - Affects install-config.yaml generation
  - Requires proper certificates and authentication

Meta:
  color: blue      # Blue for container registry/storage
  icon: database   # Database icon represents registry storage
  title: External Registry URL

Schema:
  type: string

---
Name: openshift/can-proceed
Description: Control flag for node installation progression
Documentation: |
  # Installation Progress Control

  This parameter controls whether a node can proceed with RHCOS installation
  and join the cluster. It acts as a synchronization point to ensure proper
  cluster formation sequence.

  ## Requirements
  - Must be coordinated with cluster initialization
  - Must be managed by control tasks
  - Should not be manually modified

  ## Usage
  Installation flow control:
  ```bash
  false   # Default state, node waits
  true    # Node proceeds with installation

  # Progression sequence:
  1. Node waits at openshift-wait-for-approval task
  2. Can-proceed set to true by controller
  3. Node proceeds with RHCOS installation
  4. Node continues cluster join process
  ```

  Notes:
  - Automatically managed by tasks
  - Critical for proper cluster formation
  - Ensures correct bootstrap sequence
  - Prevents premature node initialization

Meta:
  color: orange         # Orange for control/flow management
  icon: traffic light   # Traffic light icon represents proceed/wait state
  title: Installation Progress Control

Schema:
  type: boolean
  default: false

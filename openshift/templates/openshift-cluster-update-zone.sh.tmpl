#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# Set Names into DNS ZONE
cat >zone1.yaml <<EOF
Name: '{{.Machine.Name}}'
Continue: true
Description: '{{.ParamExpand "openshift/cluster-domain"}} DNS ZONE'
Meta:
  color: black
  icon: draw polygon
  title: Cluster added
Origin: '{{.ParamExpand "openshift/cluster-domain"}}.'
Priority: 0
ReadOnly: false
TTL: 3600
Records:
EOF

# Obtain IP of Load Balancer (LB)
# TODO: Consider multiple LB IPs
drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(load-balancer)' | jq '.[].Address' -r | while read ip ; do
  echo "$ip" > ip.txt
done
LB_IP=$(cat ip.txt)

job_info "Creating intial entries for Load Balancer"
cat >>zone1.yaml <<EOF
- Name: ns1
  Type: A
  Value:
    - $LB_IP
- Name: smtp
  Type: A
  Value:
    - $LB_IP
- Name: helper
  Type: A
  Value:
    - $LB_IP
- Name: helper.{{.Machine.Name}}
  Type: A
  Value:
    - $LB_IP
- Name: api.{{.Machine.Name}}
  Type: A
  Value:
    - $LB_IP
- Name: api-int.{{.Machine.Name}}
  Type: A
  Value:
    - $LB_IP
- Name: '*.apps.{{.Machine.Name}}'
  Type: A
  Value:
    - $LB_IP
EOF

job_info "Searching for bootstrap nodes"
drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(bootstrap)' | jq '.[]|.Name+" "+.Address' -r | while read name ip ; do
  job_info "Adding $name $ip"
  cat >>zone1.yaml <<EOF
- Name: ${name//.{{.ParamExpand "openshift/cluster-domain"}}}
  Type: A
  Value:
    - $ip
EOF
done

drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(controlplane)' | jq '.[]|.Name+" "+.Address' -r | while read name ip ; do
  job_info "Adding $name $ip"
  cat >>zone1.yaml <<EOF
- Name: ${name//.{{.ParamExpand "openshift/cluster-domain"}}}
  Type: A
  Value:
    - $ip
EOF
done

drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(worker)' | jq '.[]|.Name+" "+.Address' -r | while read name ip ; do
  job_info "Adding $name $ip"
  cat >>zone1.yaml <<EOF
- Name: ${name//.{{.ParamExpand "openshift/cluster-domain"}}}
  Type: A
  Value:
    - $ip
EOF
done

job_info "zone config:"
cat zone1.yaml

if drpcli zones exists "{{.Machine.Name}}" &>/dev/null; then
  job_info "Zone '{{.Machine.Name}}' exists. Updating..."
  drpcli zones update "{{.Machine.Name}}" - < zone1.yaml
else
  job_info "Zone '{{.Machine.Name}}' does not exist. Creating..."
  drpcli zones create - < zone1.yaml >/dev/null
fi

job_success "${0} Complete"

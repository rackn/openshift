#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# Store the original function
eval "original_job_info() { $(declare -f job_info | tail -n +2) }"

# Redefine job_info with timestamp
function job_info() {
  local timestamp=$(date '+%Y-%m-%d %H:%M:%S')
  original_job_info "$timestamp - $@"
}

# Function to handle periodic logging of approver process
declare -A last_print_times
function periodic_job_info() {
  local message_type=$1
  local current_time=$(date +%s)
  # Handle first time message is printed
  if [[ -z ${last_print_times[$message_type]} ]] || (( current_time - ${last_print_times[$message_type]} >= 60 )); then
    job_info "$message_type approver"
    last_print_times[$message_type]=$current_time
  fi
}

# Function to stop the approver
stop_approver() {
  job_info "Stopping Approver"
  touch /tmp/exit-approver
  wait
}

# Set trap to clean up on exit
trap stop_approver EXIT

# Function for the approver process
approver_process() {
  local time_limit=3600  # 1 hour
  local start_time=$(date +%s)
  local end_time=$((start_time + time_limit))

  job_info "Starting approver (PID: $$) with time limit of ${time_limit} seconds"
  export KUBECONFIG=/root/cluster/auth/kubeconfig

  while true; do
    if [ -f /tmp/exit-approver ]; then
      job_info "Exit file detected. Stopping approver (PID: $$)"
      break
    fi

    if [ $(date +%s) -ge ${end_time} ]; then
      job_info "Approver process timed out after ${time_limit} seconds"
      break
    fi

    periodic_job_info "Working"
    oc get csr -o go-template='{{`{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}`}}' | xargs --no-run-if-empty oc adm certificate approve || true
    periodic_job_info "Sleeping"
    sleep 10
  done

  job_info "Approver process (PID: $$) has ended"
}

job_info "Create Cluster Zone"
chmod +x openshift-cluster-update-zone.sh
./openshift-cluster-update-zone.sh

job_info "Get cluster config file"
mkdir -p /root/cluster
drpcli profiles get {{.Machine.Name}} param openshift/install-config --format=yaml > /root/cluster/install-config.yaml
sed -i 's/|+/|/g' /root/cluster/install-config.yaml

job_info "Prep Cluster manifests"
openshift-install create manifests --dir /root/cluster # --log-level=debug

job_info "Prep Cluster Ignition Files"
openshift-install create ignition-configs --dir /root/cluster # --log-level=debug

job_info "Store ignition files"
drpcli profiles set {{.Machine.Name}} param openshift/bootstrap to - < /root/cluster/bootstrap.ign >/dev/null
drpcli profiles set {{.Machine.Name}} param openshift/worker to - < /root/cluster/worker.ign >/dev/null
drpcli profiles set {{.Machine.Name}} param openshift/controlplane to - < /root/cluster/master.ign >/dev/null

job_info "Store auth files"
drpcli profiles set {{.Machine.Name}} param openshift/kubeconfig to - < /root/cluster/auth/kubeconfig >/dev/null
drpcli profiles set {{.Machine.Name}} param openshift/kubeadmin-password to - < /root/cluster/auth/kubeadmin-password >/dev/null

while read id ; do
  job_info "Starting bootstrap: $id"
  drpcli machines set $id param openshift/can-proceed to true >/dev/null
  BOOTSTRAP="$BOOTSTRAP --bootstrap $(drpcli machines show $id | jq .Name -r)"
done < <(drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(bootstrap)' | jq '.[].Uuid' -r)

while read id ; do
  job_info "Starting control plane: $id"
  drpcli machines set $id param openshift/can-proceed to true >/dev/null
  MASTERS="$MASTERS --master $(drpcli machines show $id | jq .Name -r)"
done < <(drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(controlplane)' | jq '.[].Uuid' -r)

while read id ; do
  job_info "Starting worker: $id"
  drpcli machines set $id param openshift/can-proceed to true >/dev/null
done < <(drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(worker)' | jq '.[].Uuid' -r)

while read id ; do
  job_info "Waiting for Load Balancer to finish: $id"
  drpcli machines await $id 'WorkflowComplete=Eq(true)'
done < <(drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(load-balancer)' | jq '.[].Uuid' -r)

job_info "Start node csr fixing thread"
approver_process &
APPROVER_PID=$!
job_info "Approver process started with PID: ${APPROVER_PID}"

job_info "Wait for bootstrap to finish"
set +e
openshift-install --dir /root/cluster wait-for bootstrap-complete --log-level=debug
RC=$?
set -e
if [[ "$RC" != "0" ]] ; then
  openshift-install gather bootstrap --dir /root/cluster $BOOTSTRAP $MASTERS
  drpcli files upload /root/cluster/log-bundle*.tar.gz as openshift/{{.Machine.Name}}/log-bundle.tgz
  exit 1
fi

# Rename bootstrap, set to worker role to be consumed by openshift cluster.
while read id ; do
  job_info "Renaming bootstrap: $id"
  drpcli machines set $id param openshift/role to worker >/dev/null
  
  worker_names=$(drpcli machines get {{.Machine.UUID}} param openshift/workers/names --aggregate)
  worker_index=$(echo $worker_names | jq '.|length + 1')
  newname="worker${worker_index}.{{.Machine.Name}}.{{.ParamExpand "openshift/cluster-domain"}}"
  newname_unexpanded="worker${worker_index}.{{"{{"}}.Machine.Name{{"}}"}}.{{"{{"}}.ParamExpand \\\"openshift/cluster-domain\\\"{{"}}"}}"
  
  new_worker_names="$(jq ". += [ \"$newname_unexpanded\" ]" <<< "$worker_names")"
  drpcli profiles set {{.Machine.Name}} param openshift/workers/names to "${new_worker_names}"

  drpcli machines update $id "{ \"Name\": \"$newname\" }" > /dev/null
  drpcli machines set $id param openshift/can-proceed to true >/dev/null
  drpcli machines workflow $id universal-discover > /dev/null
done < <(drpcli machines list 'aggregate=true' 'Profiles=Eq({{.Machine.Name}})' 'Params.openshift/role=Eq(bootstrap)' | jq '.[].Uuid' -r)

drpcli profiles set {{.Machine.Name}} param openshift/bootstraps/names to "[]"

./openshift-cluster-update-zone.sh
chmod +x openshift-cluster-reload-load-balancer.sh
./openshift-cluster-reload-load-balancer.sh &

job_info "Wait for completion to finish"
set +e
openshift-install --dir /root/cluster wait-for install-complete --log-level=debug
RC=$?
set -e
if [[ "$RC" != "0" ]] ; then
  openshift-install gather bootstrap --dir /root/cluster $BOOTSTRAP $MASTERS
  drpcli files upload /root/cluster/log-bundle*.tar.gz as openshift/{{.Machine.Name}}/log-bundle.tgz
  exit 1
fi

# Set OpenShift console URL param
job_info "Setting OpenShift console URL param..."
export KUBECONFIG=/root/cluster/auth/kubeconfig
console_route=$(oc get route console -n openshift-console -o jsonpath='{.spec.host}')
if [ -n "$console_route" ]; then
  job_info "Setting OpenShift console URL set to: https://${console_route}"
  drpcli profiles set {{.Machine.Name}} param openshift/console to "'https://${console_route}'"
else
  job_warn "Unable to retrieve OpenShift console URL. The param was not set."
fi

job_success "${0} Complete"

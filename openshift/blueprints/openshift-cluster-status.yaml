---
Name: openshift-cluster-status
Description: A blueprint for checking comprehensive status information about an OpenShift cluster
Documentation: |
  # OpenShift Cluster Status Blueprint

  This blueprint orchestrates the collection and reporting of comprehensive status
  information about an OpenShift cluster. It provides insights into cluster health,
  resource usage, and component status.

  ## Requirements

  ### Access Requirements
  - Cluster admin privileges
  - Valid kubeconfig
  - Metrics access
  - ACM namespace access (if installed)

  ### Cluster State
  - Operational API server
  - Running metrics components
  - Active cluster operators
  - Functioning monitoring stack

  ## Usage

  Select this blueprint from the cluster management menu when you need to assess
  cluster health and performance. Common scenarios include:
  
  - Regular health checks
  - Performance monitoring
  - Troubleshooting issues
  - Capacity planning
  - Post-upgrade verification

  ### Verification
  The blueprint provides status information about:
  - General cluster health
  - Node status and resources
  - Image storage metrics
  - ACM component status
  - Critical operator states

  ### Status Process
  The blueprint executes these tasks in sequence:
  1. openshift-cluster-status:
     - Gathers cluster metrics
     - Checks component health
     - Verifies operator status
     - Reports resource utilization

Meta:
  color: yellow    # Yellow for monitoring operations
  feature-flags: sane-exit-codes
  icon: chart bar # Chart bar icon represents status/metrics
  type: os       # Operating system level task

Tasks:
  - openshift-cluster-status

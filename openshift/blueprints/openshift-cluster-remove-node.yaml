---
Name: openshift-cluster-remove-node
Description: A blueprint for safely removing nodes from an OpenShift cluster
Documentation: |
  # OpenShift Remove Node Blueprint

  This blueprint orchestrates the safe removal of nodes from an OpenShift cluster,
  ensuring proper workload evacuation and resource cleanup. It manages the complete
  node removal process while maintaining cluster stability.

  ## Requirements

  ### Cluster State
  - Sufficient capacity on remaining nodes
  - No critical workloads on target node
  - Valid node drain options
  - Cluster admin access

  ### Infrastructure
  - Active cluster connection
  - Working cluster networking
  - Valid kubeconfig
  - Node management access

  ## Usage

  Select this blueprint from the cluster management menu when you need to remove
  nodes from your cluster. Common scenarios include:
  
  - Cluster downsizing
  - Hardware maintenance
  - Node replacement
  - Infrastructure updates
  - Resource reallocation

  ### Verification
  Before and after node removal, verify:
  - Workload migration status
  - Remaining cluster capacity
  - Resource availability
  - Node list accuracy

  ### Removal Process
  The blueprint executes these tasks in sequence:
  1. openshift-cluster-remove-node:
     - Cordons target node
     - Drains workloads
     - Removes node from cluster
     - Updates cluster configuration

Meta:
  color: yellow    # Yellow for cluster maintenance operations
  feature-flags: sane-exit-codes
  icon: unlink    # Unlink icon represents node removal
  type: os       # Operating system level task

Tasks:
  - openshift-cluster-remove-node
